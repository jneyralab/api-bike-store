<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211115000713 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE bike_types_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bikes_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bonus_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE clients_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE payments_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE rents_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE return_products_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE tarrif_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bike_types (id INT NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE bikes (id INT NOT NULL, tarrif_id INT NOT NULL, bike_type_id INT NOT NULL, model VARCHAR(255) NOT NULL, availability BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F6FAF01C7975EA5B ON bikes (tarrif_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F6FAF01C7FF015AE ON bikes (bike_type_id)');
        $this->addSql('CREATE TABLE bonus (id INT NOT NULL, return_product_id INT NOT NULL, client_id INT NOT NULL, bonus INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9F987F7A8E10CC68 ON bonus (return_product_id)');
        $this->addSql('CREATE INDEX IDX_9F987F7A19EB6921 ON bonus (client_id)');
        $this->addSql('CREATE TABLE clients (id INT NOT NULL, name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, age INT NOT NULL, national_id VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN clients.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE payments (id INT NOT NULL, total INT NOT NULL, extra INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE rents (id INT NOT NULL, owner_id INT NOT NULL, bike_id INT NOT NULL, payment_id INT NOT NULL, deposit BOOLEAN NOT NULL, payment_method VARCHAR(255) NOT NULL, start_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, completed BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_89DE39DD7E3C61F9 ON rents (owner_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_89DE39DDD5A4816F ON rents (bike_id)');
        $this->addSql('CREATE INDEX IDX_89DE39DD4C3A3BB ON rents (payment_id)');
        $this->addSql('COMMENT ON COLUMN rents.start_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN rents.end_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN rents.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE return_products (id INT NOT NULL, rent_id INT NOT NULL, description TEXT DEFAULT NULL, comments TEXT DEFAULT NULL, sanction TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A5C0BD0DE5FD6250 ON return_products (rent_id)');
        $this->addSql('COMMENT ON COLUMN return_products.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE tarrif (id INT NOT NULL, base_price INT NOT NULL, base_days INT NOT NULL, premium BOOLEAN NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE bikes ADD CONSTRAINT FK_F6FAF01C7975EA5B FOREIGN KEY (tarrif_id) REFERENCES tarrif (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bikes ADD CONSTRAINT FK_F6FAF01C7FF015AE FOREIGN KEY (bike_type_id) REFERENCES bike_types (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus ADD CONSTRAINT FK_9F987F7A8E10CC68 FOREIGN KEY (return_product_id) REFERENCES return_products (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus ADD CONSTRAINT FK_9F987F7A19EB6921 FOREIGN KEY (client_id) REFERENCES clients (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rents ADD CONSTRAINT FK_89DE39DD7E3C61F9 FOREIGN KEY (owner_id) REFERENCES clients (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rents ADD CONSTRAINT FK_89DE39DDD5A4816F FOREIGN KEY (bike_id) REFERENCES bikes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rents ADD CONSTRAINT FK_89DE39DD4C3A3BB FOREIGN KEY (payment_id) REFERENCES payments (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE return_products ADD CONSTRAINT FK_A5C0BD0DE5FD6250 FOREIGN KEY (rent_id) REFERENCES rents (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE bikes DROP CONSTRAINT FK_F6FAF01C7FF015AE');
        $this->addSql('ALTER TABLE rents DROP CONSTRAINT FK_89DE39DDD5A4816F');
        $this->addSql('ALTER TABLE bonus DROP CONSTRAINT FK_9F987F7A19EB6921');
        $this->addSql('ALTER TABLE rents DROP CONSTRAINT FK_89DE39DD7E3C61F9');
        $this->addSql('ALTER TABLE rents DROP CONSTRAINT FK_89DE39DD4C3A3BB');
        $this->addSql('ALTER TABLE return_products DROP CONSTRAINT FK_A5C0BD0DE5FD6250');
        $this->addSql('ALTER TABLE bonus DROP CONSTRAINT FK_9F987F7A8E10CC68');
        $this->addSql('ALTER TABLE bikes DROP CONSTRAINT FK_F6FAF01C7975EA5B');
        $this->addSql('DROP SEQUENCE bike_types_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bikes_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bonus_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE clients_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE payments_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE rents_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE return_products_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE tarrif_id_seq CASCADE');
        $this->addSql('DROP TABLE bike_types');
        $this->addSql('DROP TABLE bikes');
        $this->addSql('DROP TABLE bonus');
        $this->addSql('DROP TABLE clients');
        $this->addSql('DROP TABLE payments');
        $this->addSql('DROP TABLE rents');
        $this->addSql('DROP TABLE return_products');
        $this->addSql('DROP TABLE tarrif');
    }
}
