<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BikesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=BikesRepository::class)
 */
class Bikes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $model;

    /**
     * @ORM\Column(type="boolean")
     */
    private $availability;

    /**
     * @ORM\OneToOne(targetEntity=Rents::class, mappedBy="bike", cascade={"persist", "remove"})
     */
    private $rents;

    /**
     * @ORM\OneToOne(targetEntity=Tarrif::class, inversedBy="bikes", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $tarrif;

    /**
     * @ORM\OneToOne(targetEntity=BikeTypes::class, inversedBy="bikes", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $bikeType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getAvailability(): ?bool
    {
        return $this->availability;
    }

    public function setAvailability(bool $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getRents(): ?Rents
    {
        return $this->rents;
    }

    public function setRents(Rents $rents): self
    {
        // set the owning side of the relation if necessary
        if ($rents->getBike() !== $this) {
            $rents->setBike($this);
        }

        $this->rents = $rents;

        return $this;
    }

    public function getTarrif(): ?Tarrif
    {
        return $this->tarrif;
    }

    public function setTarrif(Tarrif $tarrif): self
    {
        $this->tarrif = $tarrif;

        return $this;
    }

    public function getBikeType(): ?BikeTypes
    {
        return $this->bikeType;
    }

    public function setBikeType(BikeTypes $bikeType): self
    {
        $this->bikeType = $bikeType;

        return $this;
    }
}
