<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BonusRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=BonusRepository::class)
 */
class Bonus
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $bonus;

    /**
     * @ORM\OneToOne(targetEntity=ReturnProducts::class, inversedBy="bonus", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $returnProduct;

    /**
     * @ORM\ManyToOne(targetEntity=Clients::class, inversedBy="bonuses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    public function setBonus(int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }

    public function getReturnProduct(): ?ReturnProducts
    {
        return $this->returnProduct;
    }

    public function setReturnProduct(ReturnProducts $returnProduct): self
    {
        $this->returnProduct = $returnProduct;

        return $this;
    }

    public function getClient(): ?Clients
    {
        return $this->client;
    }

    public function setClient(?Clients $client): self
    {
        $this->client = $client;

        return $this;
    }
}
