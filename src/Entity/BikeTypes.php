<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BikeTypesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=BikeTypesRepository::class)
 */
class BikeTypes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity=Bikes::class, mappedBy="bikeType", cascade={"persist", "remove"})
     */
    private $bikes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBikes(): ?Bikes
    {
        return $this->bikes;
    }

    public function setBikes(Bikes $bikes): self
    {
        // set the owning side of the relation if necessary
        if ($bikes->getBikeType() !== $this) {
            $bikes->setBikeType($this);
        }

        $this->bikes = $bikes;

        return $this;
    }
}
