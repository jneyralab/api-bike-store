<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RentsRepository;
use Doctrine\ORM\Mapping as ORM;
use DateTimeImmutable;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=RentsRepository::class)
 */
class Rents
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $deposit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paymentMethod;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $endAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Clients::class, inversedBy="rents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\OneToOne(targetEntity=Bikes::class, inversedBy="rents", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $bike;

    /**
     * @ORM\Column(type="boolean")
     */
    private $completed;

    /**
     * @ORM\ManyToOne(targetEntity=Payments::class, inversedBy="rents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $payment;

    /**
     * @ORM\OneToOne(targetEntity=ReturnProducts::class, mappedBy="rent", cascade={"persist", "remove"})
     */
    private $returnProducts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeposit(): ?bool
    {
        return $this->deposit;
    }

    public function setDeposit(bool $deposit): self
    {
        $this->deposit = $deposit;

        return $this;
    }

    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(string $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getStartAt(): ?DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?DateTimeImmutable
    {
        return $this->endAt;
    }

    public function setEndAt(DateTimeImmutable $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getOwner(): ?Clients
    {
        return $this->owner;
    }

    public function setOwner(?Clients $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getBike(): ?Bikes
    {
        return $this->bike;
    }

    public function setBike(Bikes $bike): self
    {
        $this->bike = $bike;

        return $this;
    }

    public function getCompleted(): ?bool
    {
        return $this->completed;
    }

    public function setCompleted(bool $completed): self
    {
        $this->completed = $completed;

        return $this;
    }

    public function getPayment(): ?Payments
    {
        return $this->payment;
    }

    public function setPayment(?Payments $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getReturnProducts(): ?ReturnProducts
    {
        return $this->returnProducts;
    }

    public function setReturnProducts(ReturnProducts $returnProducts): self
    {
        // set the owning side of the relation if necessary
        if ($returnProducts->getRent() !== $this) {
            $returnProducts->setRent($this);
        }

        $this->returnProducts = $returnProducts;

        return $this;
    }
}
