<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TarrifRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TarrifRepository::class)
 */
class Tarrif
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $basePrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $baseDays;

    /**
     * @ORM\Column(type="boolean")
     */
    private $premium;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity=Bikes::class, mappedBy="tarrif", cascade={"persist", "remove"})
     */
    private $bikes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBasePrice(): ?int
    {
        return $this->basePrice;
    }

    public function setBasePrice(int $basePrice): self
    {
        $this->basePrice = $basePrice;

        return $this;
    }

    public function getBaseDays(): ?int
    {
        return $this->baseDays;
    }

    public function setBaseDays(int $baseDays): self
    {
        $this->baseDays = $baseDays;

        return $this;
    }

    public function getPremium(): ?bool
    {
        return $this->premium;
    }

    public function setPremium(bool $premium): self
    {
        $this->premium = $premium;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBikes(): ?Bikes
    {
        return $this->bikes;
    }

    public function setBikes(Bikes $bikes): self
    {
        // set the owning side of the relation if necessary
        if ($bikes->getTarrif() !== $this) {
            $bikes->setTarrif($this);
        }

        $this->bikes = $bikes;

        return $this;
    }
}
