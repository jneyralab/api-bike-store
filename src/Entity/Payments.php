<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PaymentsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PaymentsRepository::class)
 */
class Payments
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $total;

    /**
     * @ORM\Column(type="integer")
     */
    private $extra;

    /**
     * @ORM\OneToMany(targetEntity=Rents::class, mappedBy="payment")
     */
    private $rents;

    public function __construct()
    {
        $this->rents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getExtra(): ?int
    {
        return $this->extra;
    }

    public function setExtra(int $extra): self
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * @return Collection|Rents[]
     */
    public function getRents(): Collection
    {
        return $this->rents;
    }

    public function addRent(Rents $rent): self
    {
        if (!$this->rents->contains($rent)) {
            $this->rents[] = $rent;
            $rent->setPayment($this);
        }

        return $this;
    }

    public function removeRent(Rents $rent): self
    {
        if ($this->rents->removeElement($rent)) {
            // set the owning side to null (unless already changed)
            if ($rent->getPayment() === $this) {
                $rent->setPayment(null);
            }
        }

        return $this;
    }
}
