<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReturnProductsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ReturnProductsRepository::class)
 */
class ReturnProducts
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comments;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $sanction;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity=Rents::class, inversedBy="returnProducts", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $rent;

    /**
     * @ORM\OneToOne(targetEntity=Bonus::class, mappedBy="returnProduct", cascade={"persist", "remove"})
     */
    private $bonus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getSanction(): ?string
    {
        return $this->sanction;
    }

    public function setSanction(?string $sanction): self
    {
        $this->sanction = $sanction;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getRent(): ?Rents
    {
        return $this->rent;
    }

    public function setRent(Rents $rent): self
    {
        $this->rent = $rent;

        return $this;
    }

    public function getBonus(): ?Bonus
    {
        return $this->bonus;
    }

    public function setBonus(Bonus $bonus): self
    {
        // set the owning side of the relation if necessary
        if ($bonus->getReturnProduct() !== $this) {
            $bonus->setReturnProduct($this);
        }

        $this->bonus = $bonus;

        return $this;
    }
}
