<?php

namespace App\Repository;

use App\Entity\BikeTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BikeTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method BikeTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method BikeTypes[]    findAll()
 * @method BikeTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BikeTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BikeTypes::class);
    }

    // /**
    //  * @return BikeTypes[] Returns an array of BikeTypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BikeTypes
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
