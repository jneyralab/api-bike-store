<?php

namespace App\Repository;

use App\Entity\ReturnProducts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReturnProducts|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReturnProducts|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReturnProducts[]    findAll()
 * @method ReturnProducts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReturnProductsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReturnProducts::class);
    }

    // /**
    //  * @return ReturnProducts[] Returns an array of ReturnProducts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReturnProducts
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
