<?php

namespace App\Repository;

use App\Entity\Tarrif;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tarrif|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tarrif|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tarrif[]    findAll()
 * @method Tarrif[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TarrifRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tarrif::class);
    }

    // /**
    //  * @return Tarrif[] Returns an array of Tarrif objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tarrif
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
