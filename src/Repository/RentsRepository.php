<?php

namespace App\Repository;

use App\Entity\Rents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rents|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rents|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rents[]    findAll()
 * @method Rents[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rents::class);
    }

    // /**
    //  * @return Rents[] Returns an array of Rents objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rents
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
