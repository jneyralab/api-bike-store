FROM php:fpm-stretch

LABEL maintainer="Jonathan Jorge Neyra Nieves <neyranievesjonathan@gmail.com>"

ARG USER_ID
ARG GROUP_ID

# Update the CLI packages handler
RUN apt-get update

# Postgresql Dependencies & Libs
RUN apt-get install -y libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql

# PHP & Project Libs
RUN apt-get install -y --no-install-recommends \
    git \
    sudo \
    zlib1g-dev \
    libxml2-dev \
    libzip-dev \
    unzip \
    && docker-php-ext-install \
    zip \
    intl

# Composer - Manage PHP Dependencies & Packages
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN sed -i 's/%sudo\tALL=(ALL:ALL) ALL/%sudo\tALL=(ALL:ALL) NOPASSWD:ALL/g' \
    /etc/sudoers && usermod -aG sudo www-data
RUN usermod --non-unique --uid ${USER_ID} www-data \
    && groupmod --non-unique --gid ${GROUP_ID} www-data

# Container Project
WORKDIR /api-store
COPY . .

EXPOSE 9000
