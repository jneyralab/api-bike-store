# BIKE RENT

#### Configurar Proyecto
El proyecto esta contenerizado atraves docker y orquestado con 
docker-compose, asi que debera tener instalado estas herramientas 
para poder levantar el proyecto.
Si se tienen estas herramientas este es el comando que necesitaras para  ejecutarlo:

`$ docker-compose up --build -d`

Los comandos tradicionales que se utilizan para terminar de cargar el proyecto son:

| Comando | Ubicacion | Proposito |
| ------------ | ------------ | ------------ |
| `$ docker exec -it -u www-data api-store-srv bash` | Host | Entrar una terminal del servicio de la api |
| `$ composer install` | Container | Instalar las dependencias y paquetes de Symfony |
| `$ php bin/console doctrine:migrations:migrate` | Container | Cargar las migraciones |

> Te recomiendo configurar el ambiente de kool para abreviar los comandos que se suelen utilizar en Docker.
[kool.dev](https://kool.dev/docs/getting-started/installation "Kool.dev")

Si tienes configurado kool en tu Host, podras configurar este proyecto con un solo comando. Con kool se haria de esta manera:
`$ kool run setup`

Este instala dependencias y hace las migraciones. 

Si tienes dudas de que comandos existen en este ambiente puedes darle un vitazo al archivo `kool.yml`.

#### Dependencias instaladas
Se esta utilizando **Symfony** 5.3.10, una base de datos **PostgreSQL** 13
y un balanceador de carga privada con la tecnologia **Nginx**.
Dentro de symfony tenemos instalado:
- Api Platform (Creacion de Apis)
- maker (Creacion Interactiva de Entities)
- profiler (Symfony Debug Toolbar)

> **[Fixed] Api Platform** tiene instalado swagger el cual tiene un problema
al servirce con Nginx, no se cargan las vistas pero es totalmente funcional.

> En esta ruta podemos revisar todos los endpoints disponibles en esta api: http://localhost:8090/api/docs.json
### Diseño

#### Database

Puedes encontra el diseño de la base de datos en la carpeta de `docs`

#### Endpoints

Estos endpoints pueden ser visualizados desde el navegador agregando la extension `.json` para mostrar la respuesta del endpoint
o tambien puede ser `.jsonld` si quieres ver como hydra sobrescribe un contexto de la respuesta
